package com.addcel.mx.services.utils;

import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.BufferedReader;

import com.google.gson.Gson;
import com.addcel.mx.services.model.mapper.ServiceMapper;
import com.addcel.mx.services.model.vo.CorreoVO;

public class Utils {

	private static final Logger logger = LoggerFactory.getLogger(Utils.class);
	private static final String URL_HEADER_ADDCEL = "/usr/java/resources/images/Addcel/MobileCard_header.PNG";
	private static String HOST = null;
    private static int PORT = 0;
    private static String USERNAME = null;
    private static String PASSWORD = null;
    private static String FROM = null; 
    private static Properties PROPS = null;
    private static ServiceMapper mapper;
    
    private static final String urlString = "http://192.168.75.53:8080/MailSenderAddcel/enviaCorreoAddcel"; //"http://192.168.75.53:8080/MailSenderAddcel/enviaCorreoAddcel";//
	
    
    static{
        logger.debug("Obteniendo datos de coneccion SMTP par envio correos.");
        try{
            HOST =  mapper.getParametro("@SMTP"); 
            PORT = Integer.parseInt( mapper.getParametro("@SMTP_PORT") ); 

            USERNAME =  mapper.getParametro("@SMTP_USER"); 
            PASSWORD =  mapper.getParametro("@SMTP_PWD"); 
            FROM = mapper.getParametro("@SMTP_MAIL_SEND");

            PROPS = new Properties();
            PROPS.put("mail.smtp.auth", "true");
            PROPS.put("mail.smtp.starttls.enable", "true");
            logger.debug("HOST: " + HOST);
            logger.debug("PORT: " + PORT);
            logger.debug("USERNAME: " + USERNAME);
            //logger.debug("PASSWORD: " + PASSWORD);
            
           // simbolos.setDecimalSeparator('.');
            //formato = new DecimalFormat(patronImp,simbolos);
        } catch (Exception e) {
            logger.error("ERROR - SEND MAIL: " + e.toString());
        }
    }
    
    
public static boolean sendMail(String destinatario,String asunto,String cuerpo){
        
        try {
            Session session = Session.getInstance(PROPS);

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress( FROM ));
//			message.setFrom(new InternetAddress( "addcel99@gmail.com" ));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(destinatario));
            message.setSubject(asunto);

            if(cuerpo.indexOf("cid:identifierCID00") > 0 ){
                //logger.debug("Se agrega imagen: " + URL_HEADER_ADDCEL);
                BodyPart messageBodyPart = new MimeBodyPart();

                // Set the HTML content, be sure it references the attachment
                //String htmlText = "<H1>Hello</H1>" +  "<img src=\"cid:memememe\">";

                // Set the content of the body part
                messageBodyPart.setContent(cuerpo, "text/html");

                // Create a related multi-part to combine the parts
                MimeMultipart multipart = new MimeMultipart("related");

                // Add body part to multipart
                multipart.addBodyPart(messageBodyPart);

                // Create part for the image
                messageBodyPart = new MimeBodyPart();

                // Fetch the image and associate to part
                DataSource fds = new FileDataSource(URL_HEADER_ADDCEL);
                messageBodyPart.setDataHandler(new DataHandler(fds));

                // Add a header to connect to the HTML
                messageBodyPart.setHeader("Content-ID","identifierCID00");

                // Add part to multi-part
                multipart.addBodyPart(messageBodyPart);

                // Associate multi-part with message
                message.setContent(multipart);
            }else{
                //logger.debug("Se agrega contenido HTML sin imagen...");
                // Create your new message part
                message.setContent(cuerpo, "text/html");
            }

            Transport transport = session.getTransport("smtp");
            transport.connect(HOST, PORT, USERNAME, PASSWORD);
            message.saveChanges();
//			Transport.send(message);
            transport.sendMessage(message,message.getAllRecipients());
            
            logger.debug("Correo enviado exitosamente: " + destinatario);

            return true;

        } catch (Exception e) {
            logger.error("ERROR - SEND MAIL: ", e);
            return false;
        }
    }


	public static boolean patternMatches(String email, String pattern){
		boolean flag = false;
		
		Pattern pat = Pattern.compile(pattern); //".*@(hotmail|live|msn|outlook)\\..*"
	     Matcher mat = pat.matcher(email.toLowerCase());
	     if (mat.matches()) {
	    	 flag = true;
	     }
		return flag;
	}
	
	public static void sendMail(String data) {
		String line = null;
		StringBuilder sb = new StringBuilder();
		try {
			logger.info("Iniciando proceso de envio email Microsoft. ");
			URL url = new URL(urlString);
			logger.info("Conectando con " + urlString);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

			urlConnection.setDoOutput(true);
			urlConnection.setRequestProperty("Content-Type", "application/json");
			urlConnection.setRequestProperty("Accept", "application/json");
			urlConnection.setRequestMethod("POST");

			OutputStreamWriter writter = new OutputStreamWriter(urlConnection.getOutputStream());
			writter.write(data);
			writter.flush();

			logger.info("Datos enviados, esperando respuesta");

			BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}

			logger.info("Respuesta del servidor(envio email Microsoft) " + sb.toString());
		} catch (Exception ex) {
			logger.error("Error en: sendMail, al enviar el email ", ex);
		}
	}
	
	 public static String parsePass(String pass)
	  {
	    int len = pass.length();
	    String key = "";
	    for (int i = 0; i < 32 / len; i++) {
	      key = key + pass;
	    }
	    int carry = 0;
	    while (key.length() < 32)
	    {
	      key = key + pass.charAt(carry);
	      carry++;
	    }
	    return key;
	  }
	 
	 public static String formatoMontoPayworks(String monto){
			String varTotal = "000";
			String pesos = null;
	        String centavos = null;
			if(monto.contains(".")){
	            pesos=monto.substring(0, monto.indexOf("."));
	            centavos=monto.substring(monto.indexOf(".")+1,monto.length());
	            if(centavos.length()<2){
	                centavos = centavos.concat("0");
	            }else{
	                centavos=centavos.substring(0, 2);
	            }            
	            varTotal=pesos+ "." + centavos;
	        }else{
	            varTotal=monto.concat(".00");
	        } 
			return varTotal;		
		}
    
}
