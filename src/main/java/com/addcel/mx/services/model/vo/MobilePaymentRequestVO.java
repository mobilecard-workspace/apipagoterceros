package com.addcel.mx.services.model.vo;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class MobilePaymentRequestVO {

	
	
	private String referencia;
	private String idUsuario;
	private String idProveedor;
	private int emisor;
    private String concepto;
	private String operacion;
	private double comision;
	private double monto;
	private long idTransaccion;
	private String banco;
	private int idPais;
	private String tarjeta;
	private String nombre;
	private String tarjetaT;
	private String vigencia;
	private String cvv2;
	private String email;
	private String tipoTarjeta;
	private String codigo_payw;
	private String xid;
	private String cavv;
	private String eci;
	private String referencia_payw;
	private UserVO user;
	private ClienteVO cliente;
	Parameters parameters;
	AfiliacionVO afiliacionVo;
	
	
	public String getReferencia_payw() {
		return referencia_payw;
	}


	public void setReferencia_payw(String referencia_pay) {
		this.referencia_payw = referencia_pay;
	}


	public String getCodigo_payw() {
		return codigo_payw;
	}


	public void setCodigo_payw(String codigo_payw) {
		this.codigo_payw = codigo_payw;
	}


	public String getXid() {
		return xid;
	}


	public void setXid(String xid) {
		this.xid = xid;
	}


	public String getCavv() {
		return cavv;
	}


	public void setCavv(String cavv) {
		this.cavv = cavv;
	}


	public String getEci() {
		return eci;
	}


	public void setEci(String eci) {
		this.eci = eci;
	}


	public AfiliacionVO getAfiliacionVo() {
		return afiliacionVo;
	}


	public void setAfiliacionVo(AfiliacionVO afiliacionVo) {
		this.afiliacionVo = afiliacionVo;
	}


	public Parameters getParameters() {
		return parameters;
	}


	public void setParameters(Parameters parameters) {
		this.parameters = parameters;
	}


	public ClienteVO getCliente() {
		return cliente;
	}


	public void setCliente(ClienteVO cliente) {
		this.cliente = cliente;
	}


	public UserVO getUser() {
		return user;
	}


	public void setUser(UserVO user) {
		this.user = user;
	}


	public String getTipoTarjeta() {
		return tipoTarjeta;
	}


	public void setTipoTarjeta(String tipoTarjeta) {
		this.tipoTarjeta = tipoTarjeta;
	}

	private String mes;
	
	private String anio;
	
	

	
	private String afiliacion;
	
	
	

	
	
	
	
	
	
	
	
	public MobilePaymentRequestVO() {	
	}
	

	public double getMonto() {
		return monto;
	}

	public void setMonto(double monto) {
		this.monto = monto;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTarjeta() {
		return tarjeta;
	}
	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}
	public String getVigencia() {
		return vigencia;
	}
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public String getAnio() {
		return anio;
	}
	public void setAnio(String anio) {
		this.anio = anio;
	}
	public String getCvv2() {
		return cvv2;
	}
	public void setCvv2(String cvv2) {
		this.cvv2 = cvv2;
	}


	public String getAfiliacion() {
		return afiliacion;
	}

	public void setAfiliacion(String afiliacion) {
		this.afiliacion = afiliacion;
	}
	
	/*public String toTrace(){
		return "[ID BITACORA: "+idTransaccion
				+", NOMBRE: "+nombre
				+", TIPO TARJETA: "+tipoTarjeta+"]";
//				+", AFILIACION: "+afiliacion.toString()+"]";
	}*/

	public long getIdTransaccion() {
		return idTransaccion;
	}

	public String getBanco() {
		return banco;
	}


	public void setBanco(String banco) {
		this.banco = banco;
	}


	public int getIdPais() {
		return idPais;
	}


	public void setIdPais(int idPais) {
		this.idPais = idPais;
	}


	public void setIdTransaccion(long idTransaccion) {
		this.idTransaccion = idTransaccion;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public String getIdProveedor() {
		return idProveedor;
	}

	public void setIdProveedor(String idProveedor) {
		this.idProveedor = idProveedor;
	}

	public int getEmisor() {
		return emisor;
	}

	public void setEmisor(int emisor) {
		this.emisor = emisor;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public double getComision() {
		return comision;
	}

	public void setComision(double comision) {
		this.comision = comision;
	}

	public String getTarjetaT() {
		return tarjetaT;
	}

	public void setTarjetaT(String tarjetaT) {
		this.tarjetaT = tarjetaT;
	}
	
}
