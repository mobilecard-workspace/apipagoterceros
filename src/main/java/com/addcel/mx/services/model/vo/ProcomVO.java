package com.addcel.mx.services.model.vo;

public class ProcomVO {
	
	// Atributos obligatorios
		private String total;
		private String currency;
		private String address;
		private String orderId;
		private String merchant;
		private String store;
		private String term;
		private String digest;
		private String urlBack;

		private String monto;
		private String idServicio;
		
		
		public ProcomVO(String total, String currency, String address, String orderId, String merchant, String store, String term, String digest, String urlBack) {
			this.total = total;
			this.currency = currency;
			this.address = address;
			this.orderId = orderId;
			this.merchant = merchant;
			this.store = store;
			this.term = term;
			this.digest = digest;
			this.urlBack = urlBack;						
		}
		
		
		public String getTotal() {
			return total;
		}
		public void setTotal(String total) {
			this.total = total;
		}
		public String getCurrency() {
			return currency;
		}
		public void setCurrency(String currency) {
			this.currency = currency;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public String getOrderId() {
			return orderId;
		}
		public void setOrderId(String orderId) {
			this.orderId = orderId;
		}
		public String getMerchant() {
			return merchant;
		}
		public void setMerchant(String merchant) {
			this.merchant = merchant;
		}
		public String getStore() {
			return store;
		}
		public void setStore(String store) {
			this.store = store;
		}
		public String getTerm() {
			return term;
		}
		public void setTerm(String term) {
			this.term = term;
		}
		public String getDigest() {
			return digest;
		}
		public void setDigest(String digest) {
			this.digest = digest;
		}
		public String getUrlBack() {
			return urlBack;
		}
		public void setUrlBack(String urlBack) {
			this.urlBack = urlBack;
		}
		public String getMonto() {
			return monto;
		}
		public void setMonto(String monto) {
			this.monto = monto;
		}
		public String getIdServicio() {
			return idServicio;
		}
		public void setIdServicio(String idServicio) {
			this.idServicio = idServicio;
		}
		
		

}
