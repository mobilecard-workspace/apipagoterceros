package com.addcel.mx.services.model.vo;

import org.springframework.web.bind.annotation.RequestParam;

public class Parameters {

	private long id_cliente;
	private String user;
	private String pass;
	private String referencia;
	private String descripcion; 
	private String monto;
	private String otro;
	
	public long getId_cliente() {
		return id_cliente;
	}
	public void setId_cliente(long id_cliente) {
		this.id_cliente = id_cliente;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referenca) {
		this.referencia = referenca;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getOtro() {
		return otro;
	}
	public void setOtro(String otro) {
		this.otro = otro;
	}
	
	
	
}
