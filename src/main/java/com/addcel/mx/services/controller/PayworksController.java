package com.addcel.mx.services.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.mx.services.business.Business;
import com.addcel.mx.services.model.mapper.ServiceMapper;
import com.addcel.mx.services.model.vo.ClienteVO;
import com.addcel.mx.services.model.vo.MobilePaymentRequestVO;
import com.addcel.mx.services.model.vo.Parameters;
import com.addcel.mx.services.model.vo.ProcomVO;

@Controller
public class PayworksController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PayworksController.class);
	
	  @Autowired
	  private ServiceMapper mapper;
	  
	  Parameters parameters =  new Parameters();
	  ClienteVO client = null;
	
	 /* @RequestMapping(value="/pago3DS", method=RequestMethod.POST)
	  public ModelAndView pago(HttpServletRequest request, Model model){
		 
		  
		//  Parameters parameters = (Parameters)modelo.get("parameters");
		 // ClienteVO client = (ClienteVO)modelo.get("cliente");
		  
		  String email = (String)request.getParameter("email").trim();
		  String cemail = (String)request.getParameter("cemail").trim();
		  String nombre = (String)request.getParameter("nombre").trim();
			
		  if(parameters.getMonto() == null)
			  LOGGER.debug("El monto es null****************************************************");
		  
		  String id_bitacora = (String)request.getParameter("idbitacora").trim();
		  long monto = Long.parseLong(parameters.getMonto()) + client.getComision();
		  
		  LOGGER.debug("monto---------->: " + monto);
		  
		  MobilePaymentRequestVO pago =  new MobilePaymentRequestVO();
		  pago.setAfiliacion("afiliacion");
		  pago.setNombre("José perez");
		  pago.setIdTransaccion(12345);
		  pago.setTarjetaT("tarjeta");
		  pago.setMonto(123.34);
		  
		  ModelAndView mav = new ModelAndView("payworks/pagina-3DS");
		  mav.addObject("pagoForm", pago);
		  return mav;
	  }
	  
	/*  @RequestMapping(value = "/", method=RequestMethod.POST)
		public ModelAndView home(@RequestParam String user, @RequestParam String pass, @RequestParam String referenciaSCT,
				@RequestParam String descripcionSCT, @RequestParam String monto,  @RequestParam String otro, ModelMap modelo ) {	
			LOGGER.debug("Controller");
			
			//_DAO = new DAO(mapper);
			Parameters parameters = new Parameters();
			parameters.setUser(user.trim());
			parameters.setPass(pass.trim());
			parameters.setReferencia(referenciaSCT.trim());
			parameters.setDescripcion(descripcionSCT.trim());
			parameters.setMonto(monto.trim());
			parameters.setOtro(otro.trim());
			LOGGER.debug("Controller");
			
			//ModelMap modelo = new ModelMap();
			ProcomVO procom = null;
			ModelAndView mav= null; 
			long idBitacora = 0;
			
			ClienteVO client = new Business(mapper).loginClient(parameters.getUser(), parameters.getPass());// _DAO.loginClient("SCT", "123");//mapper.getClient("SCT","123");
			//parameters.setId_cliente(client.getIdCliente());
			//client = new ClienteVO();
			//client.setEstado(1);
			if(client == null){
				mav = new ModelAndView("error");
			}
			else
				if(client.getEstado() == 0)
				{
					mav = new ModelAndView("no_disponible");
					
				}
				else
				{	
					idBitacora = new Business(mapper).insertaTransaccion(client, parameters);//_DAO.insertaTransaccion(client, parameters);//insertaTransaccion(client, parameters);
					//procom = new ProcomBusiness().comercioFin(idBitacora, monto);
					mav = new ModelAndView("pago");
					mav.addObject("idbitacora", idBitacora);
				
					
					//mav.addObject("merchant", client.getAfiliacion());
					//mav.addObject("prosa", procom);
					
					LOGGER.debug("Enviado a PAGO3DS");
				}
			
			return mav;	
		}
		
		
		
		@RequestMapping(value = "/test", method=RequestMethod.GET)
		public ModelAndView test(){
			
			return  new ModelAndView("test");
		}
*/
}
