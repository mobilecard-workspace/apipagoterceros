package com.addcel.mx.services.controller;




import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.addcel.mx.services.business.Business;
import com.addcel.mx.services.model.mapper.ServiceMapper;
import com.addcel.mx.services.model.vo.AfiliacionVO;
import com.addcel.mx.services.model.vo.ClienteVO;
import com.addcel.mx.services.model.vo.MobilePaymentRequestVO;
import com.addcel.mx.services.model.vo.Parameters;
import com.addcel.mx.services.model.vo.ProcomVO;
import com.addcel.mx.services.model.vo.UserVO;
import com.addcel.mx.services.procom.ProcomBusiness;
import com.addcel.mx.services.utils.Constantes;
import com.addcel.mx.services.utils.Utils;
import com.addcel.utils.AddcelCrypto;

import crypto.Crypto;

@Controller
public class ServiceController {
	
private static final Logger LOGGER = LoggerFactory.getLogger(ServiceController.class);
	
	@Autowired
	private ServiceMapper mapper;
	
	Parameters parameters =  new Parameters();
	ClienteVO client = null;
	
	MobilePaymentRequestVO pago;
	
	  @RequestMapping(value="/pago3DS", method=RequestMethod.POST)
	  public ModelAndView pago(HttpServletRequest request, Model modelo, @ModelAttribute("pago") MobilePaymentRequestVO pagoAtribute){ 
		 
		  
		 // LOGGER.debug("Cliente************************: " + ((ClienteVO)modelo..get("cliente")).getAfiliacion());//
		  LOGGER.debug("Cliente************************: " + pagoAtribute.getUser().getNombre());
	   
		  
		  String email = pagoAtribute.getUser().getEmail();
		  String cemail = pagoAtribute.getUser().getCmail();
		  String nombre = pagoAtribute.getUser().getNombre();
		  UserVO user = new UserVO();
		  user.setCmail(cemail);
		  user.setEmail(email);
		  user.setNombre(nombre);
		  pago.setUser(user);
		  double comision =  (Long.parseLong( pago.getParameters().getMonto())*pago.getCliente().getComision())/100d;
		  double monto = Long.parseLong(pago.getParameters().getMonto()) + comision;
		  LOGGER.debug("comision: " + comision + " Monto: " + monto);
		  pago.setMonto(monto);
		  pago.setComision(comision);
		  pago.setNombre(nombre);
		  //actualizando datos del usuer
		  new Business(mapper).actualizaBitacora(pago.getIdTransaccion(),nombre, email);
		  
		  ModelAndView mav = new ModelAndView("payworks/pagina-3DS");
		  mav.addObject("pagoForm", pago);
		  return mav;
	  }
	  
	  @RequestMapping(value="/enviaPago3DS", method=RequestMethod.POST)
	  public ModelAndView enviaPago3DS(HttpServletRequest request, Model model, @ModelAttribute("pagoForm") MobilePaymentRequestVO pagoAtribute){
		  
		  HashMap<String, String> resp = new HashMap<String, String>();
		  ModelAndView mav = null;
		  mav = new ModelAndView("payworks/comercio-send");
		  double total = 0;
		  
		  AfiliacionVO afiliacion = mapper.buscaAfiliacion(pago.getCliente().getAfiliacion());
		  
		  if(afiliacion == null)
			  LOGGER.debug("afiliacion null");
		  
		  LOGGER.debug("MES:*********** " +  pago.getTarjeta());
		  pago.setAfiliacionVo(afiliacion);
		  pago.setTarjeta(pagoAtribute.getTarjeta());
		  pago.setTarjetaT(pagoAtribute.getTipoTarjeta());
		  pago.setVigencia(pagoAtribute.getMes()+"/" +pagoAtribute.getAnio().substring(2, 4));
		  
		  pago.setCvv2(pagoAtribute.getCvv2());
		  
		 	LOGGER.info("DATOS ENVIADOS A https://eps.banorte.com/secure3d/Solucion3DSecure.htm : ");
			resp.put("ID_AFILIACION", afiliacion.getIdAfiliacion());
			resp.put("USUARIO", afiliacion.getUsuario());
			resp.put("CLAVE_USR", afiliacion.getClaveUsuario());
			resp.put("CMD_TRANS", "VENTA");
			resp.put("ID_TERMINAL", afiliacion.getIdTerminal());
			resp.put("Reference3D", pago.getIdTransaccion() + "");
			resp.put("MerchantId", afiliacion.getMerchantId());
			resp.put("ForwardPath",Constantes.URLBACK3DSPAYW); //http://199.231.161.38:8081/apiPagoTerceros/3dSecure/respuestaPayworks
//			resp.put("Expires", pago.getMes() + "/" + pago.getAnio().substring(2, 4));
			resp.put("Expires", pago.getVigencia());
			total =  pago.getMonto();//Double.parseDouble(parameters.getMonto()) + client.getComision();//pago.getMonto() + pago.getComision();
			resp.put("Total", Utils.formatoMontoPayworks(total + ""));
			resp.put("Card", pago.getTarjeta());
			resp.put("CardType", pago.getTarjetaT().equals("1") ? "VISA": "MC");// pago.getTipoTarjeta() == 1? "VISA": "MC"
		   
			mav.addObject("prosa", resp);
		  return mav;
	  }
	  
	
	//con PROCOM
	/*@RequestMapping(value="/pago3DS", method=RequestMethod.POST)
	public ModelAndView pago(HttpServletRequest request, Model model){
		ModelAndView mav= null;
		ProcomVO procom = null;
		//procom = new ProcomBusiness().comercioFin(1234567, 1);
		
		
		String email = (String)request.getParameter("email").trim();
		String cemail = (String)request.getParameter("cemail").trim();
		String nombre = (String)request.getParameter("nombre").trim();
		
		String id_bitacora = (String)request.getParameter("idbitacora").trim();
		long monto = Long.parseLong(parameters.getMonto()) + client.getComision();//(String)request.getParameter("monto").trim();
		LOGGER.debug("Monto total: " + monto);
		String merchant = client.getAfiliacion();//(String)request.getParameter("merchant").trim();
		monto = 1;//TODO quitar para produccion
		
		try{
				LOGGER.debug("ID: bitacora: " + id_bitacora);
				LOGGER.debug("MONTO: " + monto);
				
				if(!id_bitacora.isEmpty() && !email.isEmpty() && !nombre.isEmpty() && client != null)
				{
					LOGGER.info("email: ----> " + email);
					new Business(mapper).actualizaBitacora(Long.parseLong(id_bitacora),nombre, email);
					//_DAO.actualizaBitacora(Long.parseLong(id_bitacora),nombre, email);
					procom = new ProcomBusiness().comercioFin(Long.parseLong(id_bitacora), monto,merchant );
					//procom.setDigest(parameters.getReferencia());
					mav = new ModelAndView("comerciofin");//comerciofin
					mav.addObject("prosa", procom);
					mav.addObject("nombre", nombre);
					
					//return new ModelAndView("redirect:" + "https://www.procom.prosa.com.mx/eMerchant/8039159_imdm.jsp");
					//return new ModelAndView("redirect:" + "http://localhost:8080/apiPagoSCT/prueba");
				}else
				{
					mav = new ModelAndView("/");
				}
		}
		catch(Exception ex){
			LOGGER.error("ERROR AL PROCESAR DATOS DEL USUARIO" + ex.getStackTrace());
			ex.printStackTrace();
		}finally{
		
		     return mav;
		}
	}*/

	/*@RequestMapping(value = "/comercio-con", method = RequestMethod.POST)
	public ModelAndView respuestaProsa(HttpServletRequest request) {
		
		
		 String EM_Response="";
		 String EM_Total="";
		 String EM_OrderID="";
		 String EM_Merchant="";
		 String EM_Store="";
		 String EM_Term="";
		 String EM_RefNum="";
		 String EM_Auth="";
		 String EM_Digest="";
		 @SuppressWarnings("unchecked")
			Map<String, String[]> parametersR = request.getParameterMap();

			for (String key : parametersR.keySet()) {
				this.LOGGER.info(key);

				String[] vals = parametersR.get(key);
				if (key.equals("EM_Response"))
					EM_Response = vals[0];
				else if (key.equals("EM_Total"))
					EM_Total = vals[0];
				else if (key.equals("EM_OrderID"))
					EM_OrderID = vals[0];
				else if (key.equals("EM_Merchant"))
					EM_Merchant = vals[0];
				else if (key.equals("EM_Store"))
					EM_Store = vals[0];
				else if (key.equals("EM_Term"))
					EM_Term = vals[0];
				else if (key.equals("EM_RefNum"))
					EM_RefNum = vals[0];
				else if (key.equals("EM_Auth"))
					EM_Auth = vals[0];
				else if (key.equals("EM_Digest"))
					EM_Digest = vals[0];
				

				for (String val : vals)
					this.LOGGER.info(" -> " + val);
			}
		
		ModelAndView mav= null;
		if(client != null)
		{
			LOGGER.debug("Procesando respuesta prosa EM_Response: "+EM_Response +" EM_Total: "+ EM_Total+" EM_OrderID: "
					+EM_OrderID+" EM_Merchant: "+EM_Merchant+" EM_Store: "+EM_Store+" EM_Term: "+EM_Term+" EM_RefNum: "+ EM_RefNum + " EM_Auth: " + EM_Auth + " EM_Digest:" + EM_Digest);
			
			String digest = Business.digest(EM_Total + EM_OrderID + EM_Merchant + EM_Store + EM_Term + EM_RefNum + "-" +EM_Auth);
			
			if(EM_Response.equals("approved") && digest.equals(EM_Digest)){
				//PAgo exitoso
				new Business(mapper).actualizaBitacora(Long.valueOf(EM_OrderID), EM_Auth, 0, 1,EM_RefNum);
				//_DAO.actualizaBitacora(Long.valueOf(EM_OrderID), EM_Auth, 0, 1); //status = 1 aprovado
				mav = new ModelAndView("redirect");
				mav.addObject("EM_Response", EM_Response);
				mav.addObject("EM_RefNum", EM_RefNum);				
				mav.addObject("EM_Auth", EM_Auth);
				mav.addObject("total", parameters.getMonto());
				mav.addObject("descripcionSCT", parameters.getDescripcion());
				//enviar notificacion email
				//new Business(mapper).sendMail(EM_OrderID, EM_Auth);
				new Business(mapper).sendMail(EM_OrderID, EM_Auth, EM_RefNum);
				LOGGER.debug("PAGO EXITOSO: " + EM_OrderID + " email: "  );
				parameters =  new Parameters();
				client = null;
			}
			else
			{
				//pago no exitoso
				new Business(mapper).actualizaBitacora(Long.valueOf(EM_OrderID), EM_Auth, -1, 2,EM_RefNum);
				//_DAO.actualizaBitacora(Long.valueOf(EM_OrderID), EM_Auth, -1, 2); //status 2  tarjeta rechazada
				mav = new ModelAndView("redirect");
				mav.addObject("EM_Response", EM_Response);
				mav.addObject("EM_RefNum", EM_RefNum);				
				mav.addObject("EM_Auth", EM_Auth);															
				mav.addObject("total", parameters.getMonto());
				mav.addObject("referenciaSCT", parameters.getReferencia());
				mav.addObject("descripcionSCT", parameters.getDescripcion());
				
				//solo para pruebas
				//new Business(mapper).sendMail(EM_OrderID, EM_Auth, EM_RefNum);
				
				LOGGER.debug("PAGO NO EXITOSO: " + EM_OrderID );
			}
		}
		return mav;
		
	}*/
	
	@RequestMapping(value = "/comercio-con", method = RequestMethod.POST)
	public ModelAndView respuestaProsa(@RequestParam String EM_Response,
			@RequestParam String EM_Total, @RequestParam String EM_OrderID,
			@RequestParam String EM_Merchant, @RequestParam String EM_Store,
			@RequestParam String EM_Term, @RequestParam String EM_RefNum,
			@RequestParam String EM_Auth, @RequestParam String EM_Digest, @RequestParam String cc_numberback) {
		
		ModelAndView mav= null;
		if(client != null)
		{
			LOGGER.debug("Procesando respuesta prosa EM_Response: "+EM_Response +" EM_Total: "+ EM_Total+" EM_OrderID: "
					+EM_OrderID+" EM_Merchant: "+EM_Merchant+" EM_Store: "+EM_Store+" EM_Term: "+EM_Term+" EM_RefNum: "+ EM_RefNum + " EM_Auth: " + EM_Auth + " EM_Digest:" + EM_Digest);
			
			String digest = Business.digest(EM_Total + EM_OrderID + EM_Merchant + EM_Store + EM_Term + EM_RefNum + "-" +EM_Auth);
			
			if(EM_Response.equals("approved") && digest.equals(EM_Digest)){
				//PAgo exitoso
				//AddcelCrypto.decryptSensitive(json)
				
				new Business(mapper).actualizaBitacora(Long.valueOf(EM_OrderID), EM_Auth, 0, 1,EM_RefNum, AddcelCrypto.encryptSensitive(Constantes.key, cc_numberback), pago);//Crypto.aesEncrypt(Utils.parsePass(Constantes.key), cc_numberback));
				//_DAO.actualizaBitacora(Long.valueOf(EM_OrderID), EM_Auth, 0, 1); //status = 1 aprovado
				mav = new ModelAndView("redirect");
				mav.addObject("EM_Response", EM_Response);
				mav.addObject("EM_RefNum", EM_RefNum);				
				mav.addObject("EM_Auth", EM_Auth);
				mav.addObject("total", parameters.getMonto());
				mav.addObject("descripcionSCT", parameters.getDescripcion());
				//enviar notificacion email
				//new Business(mapper).sendMail(EM_OrderID, EM_Auth);
				new Business(mapper).sendMail(EM_OrderID, EM_Auth, EM_RefNum);
				LOGGER.debug("PAGO EXITOSO: " + EM_OrderID + " email: "  );
				parameters =  new Parameters();
				client = null;
			}
			else
			{
				//pago no exitoso
				new Business(mapper).actualizaBitacora(Long.valueOf(EM_OrderID), EM_Auth, -1, 2,EM_RefNum, AddcelCrypto.encryptSensitive(Constantes.key, cc_numberback), pago);
				//_DAO.actualizaBitacora(Long.valueOf(EM_OrderID), EM_Auth, -1, 2); //status 2  tarjeta rechazada
				mav = new ModelAndView("redirect");
				mav.addObject("EM_Response", EM_Response);
				mav.addObject("EM_RefNum", EM_RefNum);				
				mav.addObject("EM_Auth", EM_Auth);															
				mav.addObject("total", parameters.getMonto());
				mav.addObject("referenciaSCT", parameters.getReferencia());
				mav.addObject("descripcionSCT", parameters.getDescripcion());
				
				//solo para pruebas
				//new Business(mapper).sendMail(EM_OrderID, EM_Auth, EM_RefNum);
				
				LOGGER.debug("PAGO NO EXITOSO: " + EM_OrderID );
			}
		}
		return mav;
		
	}
	
	@RequestMapping(value = "/3dSecure/respuestaPayworks",  method = RequestMethod.POST)
	public ModelAndView payworksRec3DRespuesta(@RequestParam String Status,@RequestParam String MerchantCity ,@RequestParam String Number ,
											   @RequestParam String MerchantName,@RequestParam String CardType ,@RequestParam String Total ,
											   @RequestParam String Expires,@RequestParam String MerchantId ,@RequestParam String Reference3D ,ModelMap modelo, HttpServletRequest request) {
		
		HashMap<String, Object> resp = new HashMap<String, Object>();
		ModelAndView mav = null;
		Map<String, String[]> parametersq = request.getParameterMap();
		for (String key : parametersq.keySet()) {
			LOGGER.debug(key);
			String[] vals = parametersq.get(key);
			resp.put(key, vals[0]);
			for (String val : vals)
				LOGGER.debug(" -> " + val);
		}
		
		LOGGER.debug("respuesta: " + "Status: " + Status + " " + "MerchantCity: " + MerchantCity + " " + "Number: " + Number + " " + "MerchantName: " + MerchantName + " " + "CardType: " + CardType + " " + "Total: " + Total + " " + " " + "Expires: " + Expires + " " + "MerchantId: " + MerchantId + " "  + "Reference3D: " + Reference3D + " ");
		
		//Status=436&MerchantCity=Monterrey&Number=1111111122221111&MerchantName=TRANSACTO&CardType=&Total=10.0&Expires=04%2F20&MerchantId=7753116&Reference3D=121
		
		
		if( Status != null && "200".equals(Status) ){
			
			mav=new ModelAndView("payworks/iaveComercioPAYW2");
			mav.addObject("prosa", resp);
			resp.put("ID_AFILIACION", pago.getAfiliacionVo().getIdAfiliacion());
			resp.put("USUARIO", pago.getAfiliacionVo().getUsuario());
			resp.put("CLAVE_USR",pago.getAfiliacionVo().getClaveUsuario());
			resp.put("CMD_TRANS", "VENTA");
			resp.put("ID_TERMINAL", pago.getAfiliacionVo().getIdTerminal());
			resp.put("Total", Utils.formatoMontoPayworks((String)resp.get("Total")));
			resp.put("MODO", "PRD");
			resp.put("NUMERO_CONTROL", resp.get("Reference3D"));
			resp.put("NUMERO_TARJETA", resp.get("Number"));
			resp.put("FECHA_EXP", ((String)resp.get("Expires")).replaceAll("%2F", ""));
			resp.put("CODIGO_SEGURIDAD", pago.getCvv2()); 
			resp.put("MODO_ENTRADA", "MANUAL");
			resp.put("URL_RESPUESTA", Constantes.URLBACKPAY); //"http://199.231.161.38:8081/apiPagoTerceros/payworks/respuesta
			resp.put("IDIOMA_RESPUESTA", "ES");
			
			if(resp.get("XID") != null){
				resp.put("XID", ((String)resp.get("XID")).replaceAll("%3D", "="));
				resp.put("CAVV", ((String)resp.get("CAVV")).replaceAll("%3D", "="));
				pago.setXid((String)resp.get("XID"));
				pago.setCavv((String)resp.get("CAVV"));
			}
			resp.put("CODIGO_SEGURIDAD", pago.getCvv2());
			if(resp.get("ECI") != null) pago.setEci((String)resp.get("ECI"));
				
			LOGGER.info("DATOS ENVIADO A PAYWORK2: "+resp.toString());
		}
		else
		{
			new Business(mapper).actualizaBitacora(pago.getIdTransaccion(), "0000", -1, Integer.parseInt(Status),"0000", AddcelCrypto.encryptHard(pago.getTarjeta()),pago);
			mav = new ModelAndView("redirect");
			mav.addObject("EM_Response", "denied");
			mav.addObject("EM_RefNum", "0000");				
			mav.addObject("EM_Auth", "0000");															
			mav.addObject("total",  pago.getMonto());
			mav.addObject("referenciaSCT", pago.getParameters().getReferencia());
			mav.addObject("descripcionSCT", pago.getParameters().getDescripcion());
			mav.addObject("url_response", pago.getCliente().getUrl_respuesta());
			//solo para pruebas
			//new Business(mapper).sendMail(EM_OrderID, EM_Auth, EM_RefNum);
			
			
			//LOGGER.debug("Enviando correo***********************************************************");
			//new Business(mapper).sendMail(pago.getIdTransaccion()+"", "EM_Auth", "EM_RefNum");
			LOGGER.debug("PAGO NO EXITOSO 3DS: " + pago.getIdTransaccion());
		}
		
		
		
		/*ModelAndView mav = new ModelAndView("redirect");
		mav.addObject("EM_Response", "EM_Response");
		mav.addObject("EM_RefNum", "EM_RefNum");				
		mav.addObject("EM_Auth", "EM_Auth");
		mav.addObject("total", parameters.getMonto());
		mav.addObject("descripcionSCT", parameters.getDescripcion());*/
		return mav;
	}
	
	
	
	@RequestMapping(value = "/payworks/respuesta", method = RequestMethod.GET)
	public ModelAndView payworks2RecRespuesta(
			@RequestParam String NUMERO_CONTROL,
			@RequestParam(required = false) String REFERENCIA, @RequestParam String FECHA_RSP_CTE,
			@RequestParam(required = false) String CODIGO_PAYW, @RequestParam(required = false) String RESULTADO_AUT,
			@RequestParam String TEXTO, @RequestParam String RESULTADO_PAYW, @RequestParam(required = false) String BANCO_EMISOR,
			@RequestParam String FECHA_REQ_CTE, @RequestParam(required = false) String CODIGO_AUT,
			@RequestParam String ID_AFILIACION, @RequestParam(required = false) String TIPO_TARJETA, @RequestParam(required = false) String MARCA_TARJETA, 
			ModelMap modelo,HttpServletRequest request) {
		
		
		LOGGER.info("RESPUESTA PAYWORKS: NUMERO_CONTROL: " + NUMERO_CONTROL+" REFERENCIA: " + REFERENCIA
				+" FECHA_RSP_CTE: " + FECHA_RSP_CTE+" TEXTO: " + TEXTO+" RESULTADO_AUT: " + RESULTADO_AUT
				+" RESULTADO_PAYW: " + RESULTADO_PAYW+" FECHA_REQ_CTE: " + FECHA_REQ_CTE+" CODIGO_PAYW: " + CODIGO_PAYW
				+" CODIGO_AUT: " + CODIGO_AUT+" BANCO_EMISOR: " + BANCO_EMISOR+" TIPO_TARJETA: " + TIPO_TARJETA
				+" MARCA_TARJETA: " + MARCA_TARJETA);	
		
		ModelAndView mav = new ModelAndView("redirect");
		
		if (RESULTADO_PAYW != null && "A".equals(RESULTADO_PAYW)) {
			LOGGER.debug("TRANSACCION - " + NUMERO_CONTROL+" Aprobada - AUTORIZACION - "+CODIGO_AUT);
			pago.setCodigo_payw(CODIGO_AUT);
			pago.setReferencia_payw(REFERENCIA);
			new Business(mapper).actualizaBitacora(pago.getIdTransaccion(), CODIGO_AUT, 0, 1,REFERENCIA, AddcelCrypto.encryptHard(pago.getTarjeta()),pago);
			new Business(mapper).sendMail(pago.getIdTransaccion()+"", CODIGO_AUT ,REFERENCIA);
			/*
			 * 	String ticket = "COMPRA TAG I+D AUTORIZACION: " + response.getAUTONO() + " TAG: " + transaccion.getDestino()
							+ " RECARGA DE: " + transaccion.getCargo() + " COMISION: " + transaccion.getComision();
					transaccion.setBitTicket(ticket);
					transaccion.setBitStatus(1);
					transaccion.setBitNoAutorizacion(REFERENCIA);
			 */
			mav.addObject("EM_Response", "approved");
			mav.addObject("EM_RefNum", REFERENCIA);		
			mav.addObject("EM_Auth", CODIGO_AUT );
			mav.addObject("total",  pago.getMonto());
			mav.addObject("descripcionSCT",  pago.getParameters().getDescripcion());
			mav.addObject("referenciaSCT", pago.getParameters().getReferencia());
			mav.addObject("url_response", pago.getCliente().getUrl_respuesta());
		}
		else
		{
			new Business(mapper).actualizaBitacora(pago.getIdTransaccion(), NUMERO_CONTROL, -2, 2,REFERENCIA, AddcelCrypto.encryptHard(pago.getTarjeta()), pago);
			mav.addObject("EM_Response", "denied");
			mav.addObject("EM_RefNum", REFERENCIA );				
			mav.addObject("EM_Auth",  CODIGO_AUT);															
			mav.addObject("total",  pago.getMonto());
			mav.addObject("referenciaSCT", pago.getParameters().getReferencia());
			mav.addObject("descripcionSCT", pago.getParameters().getDescripcion());
			mav.addObject("url_response", pago.getCliente().getUrl_respuesta());
		}
		
		return mav;
	}
	
	/*@RequestMapping(value = "/prueba", method={RequestMethod.GET, RequestMethod.POST} )
	public ModelAndView prueba(HttpServletRequest request, 
	        HttpServletResponse response, RedirectAttributes redirectAttributes){
		ModelAndView mav= null;
			request.setAttribute("parametro1", "Hoola mundo");
			// redirectAttributes.addFlashAttribute("parametro1", "Hoola mundo");
			mav = new ModelAndView("prueba");
			//return new ModelAndView("redirect:" + "prueba.jsp");
			return mav;
	}*/
	
	
	@RequestMapping(value = "/", method=RequestMethod.POST)
	public ModelAndView home(@RequestParam String user, @RequestParam String pass, @RequestParam String referenciaSCT,
			@RequestParam String descripcionSCT, @RequestParam String monto,  @RequestParam String otro, Model modelo ) {	
		LOGGER.debug("Controller");
		
		//_DAO = new DAO(mapper);
		//Parameters parameters = new Parameters();
		pago = new MobilePaymentRequestVO();
		parameters.setUser(user.trim());
		parameters.setPass(pass.trim());
		parameters.setReferencia(referenciaSCT.trim());
		parameters.setDescripcion(descripcionSCT.trim());
		parameters.setMonto(monto.trim());
		parameters.setOtro(otro.trim());
		
		
		//ModelMap modelo = new ModelMap();
		ProcomVO procom = null;
		ModelAndView mav= null; 
		long idBitacora = 0;
		
		client = new Business(mapper).loginClient(parameters.getUser(), parameters.getPass());// _DAO.loginClient("SCT", "123");//mapper.getClient("SCT","123");
		//parameters.setId_cliente(client.getIdCliente());
		//client = new ClienteVO();
		//client.setEstado(1);
		if(client == null){
			mav = new ModelAndView("error");
		}
		else
			if(client.getEstado() == 0)
			{
				mav = new ModelAndView("no_disponible");
				
			}
			else
			{	
				idBitacora = new Business(mapper).insertaTransaccion(client, parameters);//_DAO.insertaTransaccion(client, parameters);//insertaTransaccion(client, parameters);
				//procom = new ProcomBusiness().comercioFin(idBitacora, monto);
				
				pago.setCliente(client);
				pago.setParameters(parameters);
				pago.setIdTransaccion(idBitacora);
				mav = new ModelAndView("pago", "pago", pago);
				mav.addObject("idbitacora", idBitacora);
			}
		
		return mav;	
	}
	
	
	
	@RequestMapping(value = "/test", method=RequestMethod.GET)
	public ModelAndView test(){
		
		return  new ModelAndView("test");
		
	}
	
	
	
	//codigoError 0 aprobada, -1 proceso iniciado,...
	

/*@RequestMapping(value = { "/", "/welcome" }, method = RequestMethod.GET)
public String welcomePage(Model model) {
    model.addAttribute("title", "Welcome");
    model.addAttribute("message", "This is welcome page!");
    return "index";
}	*/

}
