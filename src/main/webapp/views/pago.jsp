<%@ page contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html lang="es">
  <head>
  
    <meta charset="utf-8">
    <meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="expires" content="-1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PAGOS</title>


    <!-- CSS -->
    <link href="data:image/x-icon;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAC/0lEQVQ4T6XS+0vVdxzH8ef78z16cq4dSIaUTcpLF/xhYadyy6ytuTLyB81LjaK8IBVESyiYxCLapW3RyLXGzrpYCt0oTjWXTbLOmpY5o2Oji9HUI5pLtvJCl3M83/cg+w/2+uX12+Onp4TqK1X/akIcYOaswqQsQX27oK8NxIC7GJk8D/UfR1ur0OfDyJQsZH45EhmFhGq/1PDZT5BIsJLfxZScg13JYIchyoUUVKM9rdCwA6Jj0ecDMD0X/bsds7YOsf8NaHBrPFZMLBLsx6SthTtnEIcTXHGw+jzsnQFvpUH+EbTnBtrswW7yYMrqR4GRbfEYdz6m/xYM9yHuIui4BNExkP09HFkC8enw4U703i/Qdwv7yh4ku/IVsD0ea1YhMukduPQ5UnoRvCWjQMEJOJAO490w2Ac9f6CpJajvK2R1LTJycbfqz+WY8UlI3kFkqBec0dCwdfSzvoPA79B7AxIXw+NO9NkgdF+Dknpk5MIXSqcPE2FB5nYkbia0HYWADyIiIGU5xKXB7VNw/wK8eIqOS4b0TTDGhYQ8OUr7WayYiWAEyauC8xvB6YTEhdD7G8xYDykfwfUfwLcTtaJh+BEsqEDCbV61awqwVhyGXyuQwhq46wUBZhZD7RoorAfnWBjoBs9cmLsFfWMCOKKQsN+rdnU+1rJKuP4jsvBT6G4C0VHAuxJWXYZAI4ydCIezIHEROrsMjAMJHd+gNO/DuJcjHQ2QsRm5fRocDpi6FPyHICETOq/A9Dxo3INOykAftoG7FAm3+1Qv70ZedyFJ85E3p8HNarAckLoG/DWgNi9bT8lDW/bDSBB1upB5HyPBqiJloAsTCTI1E3ltHLR4kOAQbLgJJ1dCzBToboZZZWhjJSQswA60QuzbyItv3lcroxQzeTYQhobPYOghPPoTKaqDq3vhSRf6uAtWHIO6CpizDh3sx/afQUK+nzR8chMyJgJH7tdobTkmd9/LIkl8Dzp8kPQB/PMAJqRiN+8HVwL2k36snG8RVVX+x/4D734yc+k8tqYAAAAASUVORK5CYII=" rel="icon" type="image/x-icon" />
    <style>
	    * {
		  /*with these codes padding and border does not increase it's width.Gives intuitive style.*/
		  -webkit-box-sizing: border-box;   
		  -moz-box-sizing: border-box;
		  box-sizing: border-box;
		  color:#33313;
		  font-family: "Arial", Helvetica, sans-serif;
		}

		body {
		   margin:0;
		   padding:0;		
		   color: white;  
		   background-color:#8C8C8C;
		}
		div#envelope{  /* para desktop*/
			width:50%;
			margin: 0 auto;
			background-color:#D3441C;
			padding:10px 0;
			border:1px solid gray;
			border-radius:10px;
		}
		 
		@media only screen and (max-width: 768px) {
			div#envelope{
			width:98%;  /* para phones */
			margin: 0 auto;
			background-color:#D3441C;
			padding:10px 0;
			border:1px solid gray;
			border-radius:10px;
		}
		}
			form{  
			width:98%;
			margin:0 10%;
			margin:0 auto;
			border:1px solid grey;
			/*padding-left: 5px;
			padding-right: 5px;
			padding-top: 5px;*/
		}
		
		 
		
		form header {
		  text-align:center;
		  font-family: 'Roboto Slab', serif;
		}
		/* Makes responsive fields.Sets size and field alignment.*/
		input[type=text],input[type=password]{
			margin-bottom: 20px;
			margin-top: 10px;
			width:100%;
			padding: 15px;
			border-radius:5px;
			border:1px solid #dbd9d6;
			font-size: 110%;
		}
		input[type=submit]
		{
			margin-bottom: 20px;
			width:100%;
			color:#000000;
			padding: 15px;
			border-radius:5px;
			background-color:#dbd9d6;
			font-weight: bold;
			font-size: 150%;
			height: 55px;
		}
		input[type=button]
		{
			margin-bottom: 20px;
			width:100%;
			padding: 15px;
			color:#000000;
			border-radius:5px;
			background-color:#dbd9d6;
			font-weight: bold;
			font-size: 150%;
		}
		textarea{
			width:100%;
			padding: 15px;
			margin-top: 10px;
			border:1px solid #7ac9b7;
			border-radius:5px; 
			margin-bottom: 20px;
			resize:none;
		}
		input[type=text]:focus, textarea:focus, select:focus {
		  border-color: #333132;
		}
		.styled-select {
			width:100%;
			overflow: hidden;
			background: #FFFFFF;
			border-radius:5px;
			border:1px solid #dbd9d6;
		}
		.styled-select select {
			font-size: 110%;
			width: 100%;
			border: 0 !important;
			padding: 15px 0px 15px 0px;
			-webkit-appearance: none;
			-moz-appearance: none;
			appearance: none;
			text-indent:  0.01px;
			text-overflow: '';
		}
		.btn-style {
		    font-family: arial;
		    font-weight: bold;
		    width: 100%;
		    opacity: 0.5;
		    border: solid 1px #e6e6e6;
		    border-radius: 3px;
		    moz-border-radius: 3px;
		    font-size: 16px;
		    padding: 1px 17px;
		    background-color: #ffffff;
		    height: 55px;
		}
		.caja{
     	border: 1px solid #D3441C;
     	border-radius: 6px;
     	padding: 45px 45px 20px;
     	margin-top: 10px;
     	/*background-color: white;*/
     	box-shadow: 0px 5px 10px #D3441C, 0 0 0 10px #D3441C inset;
     
     	}
     	.caja label {
     		display: block;
     		font-weight: bold;
     		
     	}
     	.caja div{
     		margin-bottom: 15px;
     		/* width: 400px;*/
     	}
	</style>
    
    
    <!--  <link href="https://framework-gb.cdn.gob.mx/favicon.ico" rel="shortcut icon">
    <link href="https://framework-gb.cdn.gob.mx/assets/styles/main.css" rel="stylesheet">
    
    <style type="text/css">
     
     BODY { 
     font-family: Open Sans Light; 
     font-size: 18px;
     line-height: 1.428em; 
     }
     
     	.caja{
     	border: 1px solid #545454;
     	border-radius: 6px;
     	padding: 45px 45px 20px;
     	margin-top: 10px;
     	background-color: white;
     	box-shadow: 0px 5px 10px #393C3E, 0 0 0 10px #393C3E inset;
     
     	}
     	.caja label {
     		display: block;
     		font-weight: bold;
     		
     	}
     	.caja div{
     		margin-bottom: 15px;
     		width: 400px;
     	}
     
     </style>-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> 
   <!--  <script src="../js/jquery-3.1.0.min.js" type="text/javascript"></script>    -->
   
  </head>
  <body>

    <!-- Contenido -->
    
    
      <div id="envelope" >
     
        <div id="3ds-prosa">
      	<div class="alert alert-danger" id="errordatos" style="display: none; margin-top: 10px; text-align: center;">
      	</div>
      	<div style="margin-top: 10px;"><p style="text-align: center;">Introduzca datos para recibir informaci&oacute;n al finalizar el pago</p></div>
     	 	<div>
     	 	<form:form method="post" name="form1" id="form1" action="${pageContext.request.contextPath}/pago3DS" class="caja" modelAttribute="pago">
     	 		<input type="hidden" name="idbitacora" id="idbitacora" value="${idbitacora}"/>
     	 	
     	 		<!--<input type="hidden" name="monto" id="monto" value="${monto}"/> -->
     	 		<!-- <input type="hidden" name="merchant" id="merchant" value="${merchant}"/> -->
     	 		<div class="form-group">
     	 		<label for="nombre">Nombre completo<span class="form-text form-text-error">*</span>:</label>
     	 		<form:input type="text" name="nombre" id="nombre" class="form-control form-control-error" placeholder="Ingrese su nombre" path="user.nombre"/>
     	 		
     	 		</div>
     	 		<div class="form-group">
     	 		<label for="email">Correo<span class="form-text form-text-error">*</span>:</label>
     	 	    <form:input type="text" name="email" id="email" class="form-control form-control-error" placeholder="ejemplo@dominio.com" path="user.email" onselectstart="return false" onpaste="return false;" onCopy="return false" onCut="return false" onDrag="return false" onDrop="return false"/>
     	 	    
     	 		</div>
     	 		<div class="form-group">
     	 		<label for="cemail">Confirma Correo<span class="form-text form-text-error">*</span>:</label>
     	 	    <form:input type="text" name="cemail" id="cemail" class="form-control form-control-error" placeholder="ejemplo@dominio.com" path="user.cmail" onselectstart="return false" onpaste="return false;" onCopy="return false" onCut="return false" onDrag="return false" onDrop="return false"/>
     	 	    
     	 		</div>
     	 		<div class="pull-left text-muted text-vertical-align-button">
                  * Campos obligatorios
                </div>
                <div style="text-align:right">
                	<input type="submit" id="submit" name = "submit" value="Aceptar"  class="btn-style"> 
                </div>                    
        	</form:form>
        	</div>
        	
	    </div>
      
	    <br><br>
      </div>
    

    
	 <script type="text/javascript">
    $(document).ready(function() {
    	 $('#form1').submit(function() {
    	        // Expresion regular para validar el correo
    	        var regex = /[\w-\.]{2,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}/;
    	        var reg = /^[a-zA-Z \u00e1\u00e9\u00ed\u00f3\u00fa\u00c1\u00c9\u00cd\u00d3\u00da\u00f1\u00d1]{2,45}$/;
    	        if (!reg.test($('#nombre').val()) || $('#email').val().trim() == "" || $('#cemail').val().trim() == "")
    	          {
    	        	  $("#errordatos").show().html("<b>�Error!</b><br/>Favor de introducir los campos obligatorios");
    	        	  return false;
    	          }
    	        
    	        if (!regex.test($('#email').val().trim()) || !regex.test($('#cemail').val().trim())) {
    	        
    	        	$("#errordatos").show().html("<b>�Error!</b><br/>Correo no V&aacute;lido");
    	            return false;
    	        }
    	        
    	        if(!regex.test($('#cemail').val().trim()) || $('#cemail').val().trim() !=  $('#email').val().trim())
    	        	{
    	        	    $("#errordatos").show().html("<b>�Error!</b><br/>Los correos no coinciden");
    	        		return false;
    	        	}
    	        
    	        
    	        return true;
    	    });
    	 
    	 
    	 
    	 
    	 
      });
    
    </script>
	
  </body>
</html>