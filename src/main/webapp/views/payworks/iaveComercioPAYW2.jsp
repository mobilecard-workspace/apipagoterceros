<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="expires" content="-1" />
<meta name="HandheldFriendly" content="true" />
<meta name="viewport" content="width=device-width" />
<link href="data:image/x-icon;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAC/0lEQVQ4T6XS+0vVdxzH8ef78z16cq4dSIaUTcpLF/xhYadyy6ytuTLyB81LjaK8IBVESyiYxCLapW3RyLXGzrpYCt0oTjWXTbLOmpY5o2Oji9HUI5pLtvJCl3M83/cg+w/2+uX12+Onp4TqK1X/akIcYOaswqQsQX27oK8NxIC7GJk8D/UfR1ur0OfDyJQsZH45EhmFhGq/1PDZT5BIsJLfxZScg13JYIchyoUUVKM9rdCwA6Jj0ecDMD0X/bsds7YOsf8NaHBrPFZMLBLsx6SthTtnEIcTXHGw+jzsnQFvpUH+EbTnBtrswW7yYMrqR4GRbfEYdz6m/xYM9yHuIui4BNExkP09HFkC8enw4U703i/Qdwv7yh4ku/IVsD0ea1YhMukduPQ5UnoRvCWjQMEJOJAO490w2Ac9f6CpJajvK2R1LTJycbfqz+WY8UlI3kFkqBec0dCwdfSzvoPA79B7AxIXw+NO9NkgdF+Dknpk5MIXSqcPE2FB5nYkbia0HYWADyIiIGU5xKXB7VNw/wK8eIqOS4b0TTDGhYQ8OUr7WayYiWAEyauC8xvB6YTEhdD7G8xYDykfwfUfwLcTtaJh+BEsqEDCbV61awqwVhyGXyuQwhq46wUBZhZD7RoorAfnWBjoBs9cmLsFfWMCOKKQsN+rdnU+1rJKuP4jsvBT6G4C0VHAuxJWXYZAI4ydCIezIHEROrsMjAMJHd+gNO/DuJcjHQ2QsRm5fRocDpi6FPyHICETOq/A9Dxo3INOykAftoG7FAm3+1Qv70ZedyFJ85E3p8HNarAckLoG/DWgNi9bT8lDW/bDSBB1upB5HyPBqiJloAsTCTI1E3ltHLR4kOAQbLgJJ1dCzBToboZZZWhjJSQswA60QuzbyItv3lcroxQzeTYQhobPYOghPPoTKaqDq3vhSRf6uAtWHIO6CpizDh3sx/afQUK+nzR8chMyJgJH7tdobTkmd9/LIkl8Dzp8kPQB/PMAJqRiN+8HVwL2k36snG8RVVX+x/4D734yc+k8tqYAAAAASUVORK5CYII=" rel="icon" type="image/x-icon" />
<title>Redirección Payworks</title>
<style type="text/css">
root {
	display: block;
}

html {
	margin: 0;
	font-size: 62.5%;
	color: white;
	background-color: #D3441C;
}
body {
	font-size: 14px;
    font-weight: bold;
	line-height: 1.428571429;
}

@media ( min-width :750px) {
	.container {
		width: 100%;
		margin-right: auto;
		margin-left: auto;
	}
}

@media ( min-width :992px) {
	.container {
		width: 700px;
		margin-right: auto;
		margin-left: auto;
	}
} 
</style>

<script type="text/javascript">
	function sendform() {
		if (self.name.length === 0) {
			self.name = "gotoProsa";
		}
		//var twnd = window.open("","wnd","toolbar=0,location=0,directories=0,status=0,menubar=0,resizable=1,copyhistory=0,width=760,height=750");
		//document.form1.return_target.value = self.name.toString();
		//document.form1.target = "wnd";
		document.form1.submit();
	}
</script>
</head>
<body onload=" sendform();"> <!--  -->
	<form method="post" name="form1" action="https://via.banorte.com/payw2">
		<input type="hidden" name="ID_AFILIACION" value="${prosa.ID_AFILIACION}" /> 
		<input type="hidden" name="USUARIO" value="${prosa.USUARIO}" /> 
		<input type="hidden" name="CLAVE_USR" value="${prosa.CLAVE_USR}" /> 
		<input type="hidden" name="CMD_TRANS" value="${prosa.CMD_TRANS}" /> 
		<input type="hidden" name="ID_TERMINAL" value="${prosa.ID_TERMINAL}" /> 
		<input type="hidden" name="MONTO" value="${prosa.Total}" /> 
		<input type="hidden" name="MODO" value="${prosa.MODO}" /> 
		<%-- <input type="hidden" name="REFERENCIA" value="${prosa.referencia}" />  --%>
		<input type="hidden" name="NUMERO_CONTROL" value="${prosa.Reference3D}" /> 
		<%-- <input type="hidden" name="REF_CLIENTE1" value="${prosa.REF_CLIENTE1}" /> 
		<input type="hidden" name="REF_CLIENTE2" value="${prosa.REF_CLIENTE2}" /> 
		<input type="hidden" name="REF_CLIENTE3" value="${prosa.REF_CLIENTE3}" /> 
		<input type="hidden" name="REF_CLIENTE4" value="${prosa.REF_CLIENTE4}" /> 
		<input type="hidden" name="REF_CLIENTE5" value="${prosa.REF_CLIENTE5}" /> --%> 
		<input type="hidden" name="NUMERO_TARJETA" value="${prosa.Number}" /> 
		<input type="hidden" name="FECHA_EXP" value="${prosa.FECHA_EXP}" /> 
		<input type="hidden" name="CODIGO_SEGURIDAD" value="${prosa.CODIGO_SEGURIDAD}" /> 
		<%-- <input type="hidden" name="CODIGO_AUT" value="${prosa.auth}" /> --%> 
		<input type="hidden" name="MODO_ENTRADA" value="${prosa.MODO_ENTRADA}" />
		<%-- <input type="hidden" name="LOTE" value="${prosa.lote}" /> --%>
		<input type="hidden" name="URL_RESPUESTA" value="${prosa.URL_RESPUESTA}" />
		<input type="hidden" name="IDIOMA_RESPUESTA" value="${prosa.IDIOMA_RESPUESTA}" />
		<%
		    String xid = request.getParameter("XID");
			String cavv = request.getParameter("CAVV");
		    if (xid != null || xid != "" && cavv != null || cavv != "") {
		%>
		    <input type="hidden" name="XID" value="${prosa.XID}" />
			<input type="hidden" name="CAVV" value="${prosa.CAVV}" />
		<%
		    }
		%>
		
		
		<input type="hidden" name="ESTATUS_3D" value="${prosa.Status}" />
		<input type="hidden" name="ECI" value="${prosa.ECI}" />
	</form>
	<div class="container">
		
		<br />
		<table border="0" width="100%">
			<thead>
			<tr class="cart_menu">
				<th class="description"><p>Envio de informaci&oacute;n.</p></th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td class="cart_description">
					<p><br/>Se est&aacute; enviando la informaci&oacute;n, favor de esperar. <br/>
					</p>
				</td>
			</tr>
			</tbody>
		</table>
	</div>
</body>
</html>