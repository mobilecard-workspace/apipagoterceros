<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" 		  prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.0//EN" "http://www.wapforum.org/DTD/xhtml-mobile10.dtd">
<html>

<head>
    <title>Purchase Verification</title>
    <!-- Meta Tags -->
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="expires" content="-1" />
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	
<%	
   	response.setHeader("Expires","0");
   	response.setHeader("Pragma","no-cache");
   	response.setDateHeader("Expires",-1);
%>
    	<style>
	    * {
		  /*with these codes padding and border does not increase it's width.Gives intuitive style.*/
		  -webkit-box-sizing: border-box;   
		  -moz-box-sizing: border-box;
		  box-sizing: border-box;
		  color:#33313;
		  font-family: "Arial", Helvetica, sans-serif;
		}

		body {
		   margin:0;
		   padding:0;		
		   color: white;  
		   background-color:#8C8C8C;
		}
		label{
		font-weight:bold;
		}
		div#envelope{
			width:40%;
			margin: 0 auto;
			background-color:#D3441C;
			padding:10px 0;
			border:1px solid gray;
			border-radius:10px;
		}
		@media only screen and (max-width: 768px) {
		div#envelope{
			width:90%;
			margin: 0 auto;
			background-color:#D3441C;
			padding:10px 0;
			border:1px solid gray;
			border-radius:10px;
		}
		}
		div#info{
			width:98%;
			margin:0 10%;
			margin: 0 auto;
			border-radius:5px;
			border:1px solid grey;
			padding-left: 10px;
		}  
		form{
			width:98%;
			margin:0 10%;
			margin: 0 auto;
			border-radius:5px;
			border:1px solid grey;
			padding-left: 10px;
			padding-right: 10px;
			padding-top: 5px;
		}  
		form header {
		  text-align:center;
		  font-family: 'Roboto Slab', serif;
		}
		/* Makes responsive fields.Sets size and field alignment.*/
		input[type=text],input[type=password]{
			margin-bottom: 20px;
			margin-top: 10px;
			width:100%;
			padding: 15px;
			border-radius:5px;
			border:1px solid #dbd9d6;
			font-size: 110%;
		}
		input[type=submit]
		{
			margin-bottom: 20px;
			width:100%;
			color:#000000;
			padding: 15px;
			border-radius:5px;
			background-color:#dbd9d6;
			font-weight: bold;
			font-size: 150%;
			height: 55px;
		}
		input[type=button]
		{
			margin-bottom: 20px;
			width:100%;
			padding: 15px;
			color:#000000;
			border-radius:5px;
			background-color:#dbd9d6;
			font-weight: bold;
			font-size: 150%;
		}
		textarea{
			width:100%;
			padding: 15px;
			margin-top: 10px;
			border:1px solid #7ac9b7;
			border-radius:5px; 
			margin-bottom: 20px;
			resize:none;
		}
		input[type=text]:focus, textarea:focus, select:focus {
		  border-color: #333132;
		}
		.styled-select {
			width:100%;
			overflow: hidden;
			background: #FFFFFF;
			border-radius:5px;
			border:1px solid #dbd9d6;
		}
		.styled-select select {
			font-size: 110%;
			width: 100%;
			border: 0 !important;
			padding: 15px 0px 15px 0px;
			-webkit-appearance: none;
			-moz-appearance: none;
			appearance: none;
			text-indent:  0.01px;
			text-overflow: '';
		}
		.btn-style {
		    font-family: arial;
		    font-weight: bold;
		    width: 100%;
		    opacity: 0.5;
		    border: solid 1px #e6e6e6;
		    border-radius: 3px;
		    moz-border-radius: 3px;
		    font-size: 16px;
		    padding: 1px 17px;
		    background-color: #ffffff;
		    height: 55px;
		}
	</style>
	<link href="data:image/x-icon;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAC/0lEQVQ4T6XS+0vVdxzH8ef78z16cq4dSIaUTcpLF/xhYadyy6ytuTLyB81LjaK8IBVESyiYxCLapW3RyLXGzrpYCt0oTjWXTbLOmpY5o2Oji9HUI5pLtvJCl3M83/cg+w/2+uX12+Onp4TqK1X/akIcYOaswqQsQX27oK8NxIC7GJk8D/UfR1ur0OfDyJQsZH45EhmFhGq/1PDZT5BIsJLfxZScg13JYIchyoUUVKM9rdCwA6Jj0ecDMD0X/bsds7YOsf8NaHBrPFZMLBLsx6SthTtnEIcTXHGw+jzsnQFvpUH+EbTnBtrswW7yYMrqR4GRbfEYdz6m/xYM9yHuIui4BNExkP09HFkC8enw4U703i/Qdwv7yh4ku/IVsD0ea1YhMukduPQ5UnoRvCWjQMEJOJAO490w2Ac9f6CpJajvK2R1LTJycbfqz+WY8UlI3kFkqBec0dCwdfSzvoPA79B7AxIXw+NO9NkgdF+Dknpk5MIXSqcPE2FB5nYkbia0HYWADyIiIGU5xKXB7VNw/wK8eIqOS4b0TTDGhYQ8OUr7WayYiWAEyauC8xvB6YTEhdD7G8xYDykfwfUfwLcTtaJh+BEsqEDCbV61awqwVhyGXyuQwhq46wUBZhZD7RoorAfnWBjoBs9cmLsFfWMCOKKQsN+rdnU+1rJKuP4jsvBT6G4C0VHAuxJWXYZAI4ydCIezIHEROrsMjAMJHd+gNO/DuJcjHQ2QsRm5fRocDpi6FPyHICETOq/A9Dxo3INOykAftoG7FAm3+1Qv70ZedyFJ85E3p8HNarAckLoG/DWgNi9bT8lDW/bDSBB1upB5HyPBqiJloAsTCTI1E3ltHLR4kOAQbLgJJ1dCzBToboZZZWhjJSQswA60QuzbyItv3lcroxQzeTYQhobPYOghPPoTKaqDq3vhSRf6uAtWHIO6CpizDh3sx/afQUK+nzR8chMyJgJH7tdobTkmd9/LIkl8Dzp8kPQB/PMAJqRiN+8HVwL2k36snG8RVVX+x/4D734yc+k8tqYAAAAASUVORK5CYII=" rel="icon" type="image/x-icon" />
</head>
<body>

<div id="envelope">
		<div id="info">
			<p style="text-align: center;font-weight:bold;">Informaci&oacute;n de pago</p>
			<label>Comisi&oacute;n: </label>$ ${pagoForm.comision}<br/>
			<label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total: </label>$ ${pagoForm.monto}
		</div>
		<br/>
		<form:form id="main-form" name="main-form" method="post" 
    		autocomplete="off" modelAttribute="pagoForm"
    		action="${pageContext.request.contextPath}/enviaPago3DS">
				<label for="nombre">Nombre: </label>${pagoForm.nombre}
				<br/></br>
				<p style="text-align: center; font-weight:bold;">Proporciona la siguiente informaci&oacute;n:</p>
				<!-- form:input type="text" path="nombre" size="40,1" maxlength="40" value="" required="true" /-->
				
				<label for="tarjeta">N&uacute;mero de Tarjeta:</label>
				<br/>
				<form:input type="text" path="tarjeta" size="40,1"	maxlength="16" value="" required="true" onKeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" />

				<label for="tipoTarjeta">Tipo:</label><br/>
				
				<div class="styled-select">
					<form:select path="tipoTarjeta" style="-moz-appearance: none;text-indent: 0.01px;text-overflow: '';">
						<option value="1">VISA</option>
						<option value="2">MasterCard</option>
						<!-- <option value="3">Carnet</option> -->
					</form:select  >
				</div>
				<div style="width:100%;padding-top:10px">
					<label for ="mes">Fecha de Vencimiento (mes/a&ntilde;o):</label><br/>
					<div class="styled-select" style="float:left;width:45%;">
						<form:select  path="mes" style="-moz-appearance: none;text-indent: 0.01px;text-overflow: '';">
						<%
								java.util.Calendar C = java.util.Calendar.getInstance();
								int anio = C.get(java.util.Calendar.MONTH);
								String c="0";
								for (int i = anio; i < 12; i++) {
									if(i== 9) c="";
									out.println("<option value=\""+ c+(anio +1)  +"\">" + c+(anio +1) + "</option>");
									anio++;
								}
							%>
							<!-- <option value="01">01</option>
							<option value="02">02</option>
							<option value="03">03</option>
							<option value="04">04</option>
							<option value="05">05</option>
							<option value="06">06</option>
							<option value="07">07</option>
							<option value="08">08</option>
							<option value="09">09</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option> -->
						</form:select> 
					</div>
					<div  class="styled-select" style="float:right;width:45%;">
						<form:select  path="anio" class="selmenu">
							<%
								java.util.Calendar C = java.util.Calendar.getInstance();
								int anio = C.get(java.util.Calendar.YEAR);
								for (int i = 0; i < 15; i++) {
									out.println("<option value=\""+anio+"\">" + anio+ "</option>");
									anio++;
								}
							%>
						</form:select>
					</div>
				</div>
				<div style="width:100%;padding-top:70px">
					<label for ="cvv2">C&oacute;digo de seguridad (CVV2/CVC2)</label>
					<form:input type="password" path="cvv2" size="3,1" maxlength="3" value="" required="true" class="txtinput"/>
				</div>
				<p>Verifica los datos y selecciona el bot&oacute;n Pagar para efectuar el cargo a tu tarjeta.</p>
				<div style="width:100%;padding-top:10px">
					<input type="submit" value="Pagar" class="btn-style"/>
				</div>
				<!-- <div style="width:100%;padding-top:10px">
					<input type="button" value="Cancelar" />
				</div> -->
			</div>
		</form:form>
		
	</div>
</body>
</html>
