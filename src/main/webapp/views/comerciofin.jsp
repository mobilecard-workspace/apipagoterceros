<%@ page contentType="text/html; charset=ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="es">
  <head>
    
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="Pragma" content="no-cache" />
		<meta http-equiv="expires" content="-1" />
		<meta name="HandheldFriendly" content="true" />
		<meta name="viewport" content="width=device-width" />
		
		<%	
		   	response.setHeader("Expires","0");
		   	response.setHeader("Pragma","no-cache");
		   	response.setDateHeader("Expires",-1);
		 	
		%>
    
    
    <title>PAGO GOB.mx</title>


    <!-- CSS -->
    <link href="https://framework-gb.cdn.gob.mx/favicon.ico" rel="shortcut icon">
    <link href="https://framework-gb.cdn.gob.mx/assets/styles/main.css" rel="stylesheet">
	<style type="text/css">
	
	BODY { 
     font-family: Open Sans Light; 
     font-size: 18px;
     line-height: 1.428em; 
     }
	
     	.caja{
     	border: 1px solid #545454;
     	border-radius: 6px;
     	padding: 5px 45px 30px;
     	margin-top: 10px;
     	background-color: white;
     	box-shadow: 0px 5px 10px #393C3E, 0 0 0 10px #393C3E inset;
     	width: 350px;
     	} 	
     	
     </style>
    <!-- Respond.js soporte de media queries para Internet Explorer 8 -->
    <!-- ie8.js EventTarget para cada nodo en Internet Explorer 8 -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/ie8/0.2.2/ie8.js"></script>
    <![endif]-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> 
  </head>
  <body>

    <!-- Contenido -->
    
    <main class="page">
      <div class="container">
      <input type="hidden" name="name" value="${nombre}" />
      	<div id="3ds-prosa" style="width: 400px; margin: auto;">
     	 	<c:import var="testHtml"
			url="https://www.procom.prosa.com.mx/eMerchant/8039159_imdm.jsp"/>						
			<c:out value="${testHtml}" escapeXml="false" />
			
			<script type="text/javascript">

		
		var frm = document.getElementsByTagName('form')[0];
		
		frm.innerHTML = frm.innerHTML+
		'<input type="hidden" name="total" value="${prosa.total}">'+
        '<input type="hidden" name="currency" value="${prosa.currency}">'+
        '<input type="hidden" name="address" value="${prosa.address}">'+
        '<input type="hidden" name="order_id" value="${prosa.orderId}">'+
        '<input type="hidden" name="merchant" value="${prosa.merchant}">'+
        '<input type="hidden" name="store" value="${prosa.store}">'+
        '<input type="hidden" name="term" value="${prosa.term}">'+
        '<input type="hidden" name="digest" value="${prosa.digest}">'+
        '<input type="hidden" name="return_target" value="">'+                       
        '<input type="hidden" name="urlBack" value="${prosa.urlBack}">'; 
        
        frm.setAttribute('action','https://www.procom.prosa.com.mx/eMerchant/validaciones/valida.do');
        frm.setAttribute('class','caja');
      //Establecer valores
	  document.getElementsByName('cc_name')[0].style.visibility="hidden";	
      document.getElementsByName('cc_name')[0].value = document.getElementsByName('name')[0].value;
     
	  var tbl = document.getElementsByTagName('table')[0];
		tbl.deleteRow(0);
		
		</script>
		
	    </div>
      </div>
      <br><br>
    </main>

    <!-- 
    <script src="https://framework-gb.cdn.gob.mx/gobmx.js"></script>JS -->
     <script type="text/javascript">
    $(document).ready(function() {
    	
    	  $("input:text[name=cc_number]").change(function(){
    		 // alert('holaaa');
    		  document.getElementsByName('cc_numberback')[0].value = $(this).val();
             // $('input:text[name=cc_numberback]').val($(this).val());
            // alert(document.getElementsByName('cc_numberback')[0].value);
  			}); 
  		 
    	 
      });
    
    </script>
    
  

  </body>
</html>